import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    items: [],
    nextId: 0,
  },

  mutations: {
    addItems(state, payload) {
      const names = payload.names;
      names.forEach(name => addItem(state, name, payload.priority));
    },

    removeItem(state, item) {
      state.items = state.items.filter(value => value.id !== item.id);
    },

    changeStatus(state, item) {
      const toChange = state.items.find(value => value.id === item.id);
      toChange.done = !toChange.done;
    },
  },
});

function addItem(state, name, priority) {
  const item = {
    id: state.nextId,
    name: name,
    priority: priority,
    done: false,
  };

  state.items.push(item);
  state.nextId = state.nextId + 1;
}
